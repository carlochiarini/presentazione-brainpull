let scene, camera, renderer, cone, plane, pointLight, pointLight2, sphere, sphere2;

let ADD = 0.09;

let createGeometry = function() {
    
    let geometry = new THREE.ConeGeometry(4, 10, 9);
    let material = new THREE.MeshPhongMaterial({color: 0X162BCA, shininess: 100, side: THREE.DoubleSide});
    cone = new THREE.Mesh(geometry, material);
    cone.position.x = 0;
    cone.position.y = -4;
    cone.position.z = -5;
    
    geometry = new THREE.PlaneGeometry(1000, 1000, 50, 50);
    material = new THREE.MeshPhongMaterial({color: 0X888888, side: THREE.DoubleSide});
    plane = new THREE.Mesh(geometry, material);
    plane.rotation.x = Math.PI / 2;
    plane.position.y = -100;

    
    geometry = new THREE.SphereGeometry(1, 30, 30);
    material = new THREE.MeshBasicMaterial({color: 0xffd700});
    sphere = new THREE.Mesh(geometry, material);
    sphere.position.x = 10;
    sphere.position.z = 0;
    sphere.position.y = 5;

    geometry = new THREE.SphereGeometry(1, 30, 30);
    material = new THREE.MeshBasicMaterial({color: 0xffd700});
    sphere2 = new THREE.Mesh(geometry, material);
    sphere2.position.x = -10;
    sphere2.position.z = 0;
    sphere2.position.y = 5;

    scene.add(cone);
    scene.add(plane);
    scene.add(sphere);
    scene.add(sphere2);
};


// set up the environment - 
// initiallize scene, camera, objects and renderer
let init = function() {
    // create the scene
    scene = new THREE.Scene();
    scene.background = new THREE.Color(0xffffff);
    
    // create an locate the camera
    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1000);
    camera.position.z = 20;
    
    // light = new THREE.DirectionalLight(0xffffff);
    // light.position.y = 5;
    // light.position.z = 0;
    // light.position.x = 10;
    
    var pointLight = new THREE.PointLight( 0xFFFFFF );
    pointLight.position.x = 0;
    pointLight.position.y = 400;
    pointLight.position.z = 40;
    pointLight.intensity = 1.5;
    pointLight.castShadow = true;
    scene.add(pointLight);


    var pointLight2 = new THREE.PointLight( 0xFFFFFF );
    pointLight2.position.x = 900;
    pointLight2.position.y = -400;
    pointLight2.position.z = 130;
    pointLight2.intensity = 1.5;
    pointLight2.castShadow = true;
    scene.add(pointLight2);

    // scene.add(light);
    
    
    //    lightHelper = new THREE.DirectionalLightHelper(light, 5, 0x000000);
    //     scene.add(lightHelper);
    
    createGeometry();
    
    //light.target = cone;
    // create the renderer   
    renderer = new THREE.WebGLRenderer();   
    renderer.setSize(window.innerWidth, window.innerHeight);
    
    document.body.appendChild(renderer.domElement);
    
};


// main animation loop - calls 50-60 times per second.
let mainLoop = function() {
    
    // pointLight.position.x += ADD;
    // pointLight2.position.x += ADD;
    // sphere.position.x += ADD;
    // if(pointLight.position.x > 10 || pointLight.position.x < -10 && pointLight2.position.x > 10 || pointLight2.position.x < -10)
    // ADD *= -1;

    
    // lightHelper.update();
    
    renderer.render(scene, camera);
    requestAnimationFrame(mainLoop);
};

///////////////////////////////////////////////
init();
mainLoop();