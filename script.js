// Navbar

document.addEventListener('scroll', () =>{
    let navbar = document.querySelector('#navbar')

    if(window.pageYOffset > 80){
        navbar.classList.add('shadow-lg')
    }
    else{
        navbar.classList.remove('shadow')
    }
})


// Traccia 2

var locations = [
    ["Piazza Luigi di Savoia 1 - Milano", 45.48679, 9.20678],
    ["Via Togliatti, 2 - Milano", 45.52998, 9.36663],
    ["Piazza Venticinque Aprile - Milano", 45.48034, 9.18722],
    ["Via Emilia Ovest 1480 - Modena", 44.64792, 10.85470],
    ["Via Emilia Est 981 - Modena", 44.63459, 10.95522],
    ["Via Orabona 4 - Bari", 41.10750, 16.87900],
    ["Piazza Ferrarese 28 - Bari", 41.12752, 16.87190]
];

var map = L.map('map').setView([41.458, 12.706], 5);
mapLink =
'<a href="http://openstreetmap.org">OpenStreetMap</a>';
L.tileLayer(
    'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; ' + mapLink,
    maxZoom: 18,
}).addTo(map);

for (var i = 0; i < locations.length; i++) {
    marker = new L.marker([locations[i][1], locations[i][2]])
    .bindPopup(locations[i][0])
    .addTo(map);
    
}



// let list = document.querySelector("#elenco");

// const places = [
//     "Piazza Luigi di Savoia 1 - Milano",
//     "Via Togliatti, 2 - Milano",
//     "Piazza Venticinque Aprile - Milano",
//     "Via Emilia Ovest 1480 - Modena",
//     "Via Emilia Est 981 - Modena",
//     "Via Orabona 4 - Bari",
//     "Piazza Ferrarese 28 - Bari"
// ];


// for (let i = 0; i < places.length; i++) {
    
//     let newItem = document.createElement("li");
//     newItem.innerHTML = '<i class="fal fa-map-marker">' + " " + places[i];
//     list.appendChild(newItem);
    
// }

// AOS

AOS.init();