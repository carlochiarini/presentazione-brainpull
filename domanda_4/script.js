let title = document.querySelector('#title');
let fly = title.innerHTML.split("");
let space = "";

for (let i = 0; i < fly.length; i++) {
    if (fly[i] != " "){
        space +='<div style="position:relative; display:inline-block;">' + fly[i] + '</div>';
    } else {
        space += fly[i];
    }
    
}

title.innerHTML = space;
let names = title.getElementsByTagName("div");

let tl = gsap.timeline({
    repeatDelay: 1,
    yoyo : true
});

tl.set("#title", {prespective : 400});
tl.from(names,{
    duration: 0.3,
    opacity: 0,
    x:gsap.utils.random(-300, 300, true),
    y:gsap.utils.random(-300, 300, true),
    z:gsap.utils.random(-300, 300, true),
    

    stagger: {
        amount: 3
    }
})


